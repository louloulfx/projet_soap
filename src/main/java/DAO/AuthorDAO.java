package DAO;

import entities.Author;
import entities.Book;
import entities.Books;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class AuthorDAO {

    private Author buildAuthorWithBookList(Connection connection, ResultSet rs) throws SQLException {
        PreparedStatement statement = connection.prepareStatement("select * from book where author_id = ?;");
        statement.setInt(1, rs.getInt("rowid"));
        ResultSet rs2 = statement.executeQuery();
        ArrayList<Book> books = new ArrayList<Book>();
        while (rs2.next()) {
            Book book = new Book(rs2.getString("isbn"), rs2.getString("title"), null);
            books.add(book);
        }
        return new Author(rs.getInt("rowid"), rs.getString("prenom"), rs.getString("nom"), new Books(books));
    }

    public ArrayList<Author> getAll() throws SQLException {
        Connection connection = Database.getInstance().getConnection();
        PreparedStatement statement = connection.prepareStatement("select rowid, prenom, nom from author;");
        ResultSet rs = statement.executeQuery();
        ArrayList<Author> authors = new ArrayList<Author>();
        while (rs.next()) {
            Author author = buildAuthorWithBookList(connection, rs);
            authors.add(author);
        }
        return authors;
    }

    public Author getById(Integer id) throws SQLException {
        Connection connection = Database.getInstance().getConnection();
        PreparedStatement statement = connection
                .prepareStatement("select rowid, prenom, nom from author where author.rowid = ?;");
        statement.setInt(1, id);
        ResultSet rs = statement.executeQuery();
        return buildAuthorWithBookList(connection, rs);
    }

    public int create(String prenom, String nom) throws SQLException {
        Connection connection = Database.getInstance().getConnection();
        PreparedStatement statement = connection
                .prepareStatement("insert into author (prenom, nom) values (?, ?);");
        statement.setString(1, prenom);
        statement.setString(2, nom);
        return statement.executeUpdate();
    }

    public int update(Integer id, String prenom, String nom) throws SQLException {
        Connection connection = Database.getInstance().getConnection();
        PreparedStatement statement = connection
                .prepareStatement("update author set prenom = ?, nom = ? where author.rowid = ?;");
        statement.setString(1, prenom);
        statement.setString(2, nom);
        statement.setInt(3, id);
        return statement.executeUpdate();
    }

    public int delete(Integer id) throws SQLException {
        Connection connection = Database.getInstance().getConnection();
        PreparedStatement statement = connection.prepareStatement("delete from author where author.rowid = ?;");
        statement.setInt(1, id);
        return statement.executeUpdate();
    }
}
