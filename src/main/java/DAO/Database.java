package DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class Database {
    private static Database instance;
    private Connection connection;

    private Database() {
        try {
            this.connection = DriverManager.getConnection("jdbc:sqlite:database.db");
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        }
    }

    public static Database getInstance() throws SQLException {
        if (instance == null) {
            instance = new Database();
        } else if (instance.getConnection().isClosed()) {
            instance = new Database();
        }
        return instance;
    }

    public Connection getConnection() {
        return this.connection;
    }

    public void populate() {
        try {
            String[] populate = {
                    "drop table if exists author;",
                    "drop table if exists book;",
                    "create table author (prenom text not null, nom text not null);",
                    "create table book (isbn text primary key, title text not null, author_id integer not null, foreign key (author_id) references author(rowid));",
                    "insert into author (prenom, nom) values ('Auteur_prénom_1', 'Auteur_prénom_1');",
                    "insert into author (prenom, nom) values ('Auteur_prénom_2', 'Auteur_prénom_2');",
                    "insert into author (prenom, nom) values ('Auteur_prénom_3', 'Auteur_prénom_3');",
                    "insert into author (prenom, nom) values ('Auteur_prénom_4', 'Auteur_prénom_4');",
                    "insert into book (ISBN, title, author_id) values ('8684486486461', 'Livre 1', 1);",
                    "insert into book (ISBN, title, author_id) values ('265982151', 'Livre 2', 1);",
                    "insert into book (ISBN, title, author_id) values ('12584212', 'Livre 3', 2);",
                    "insert into book (ISBN, title, author_id) values ('845121', 'Livre 4', 2);",
                    "insert into book (ISBN, title, author_id) values ('8451484511', 'Livre 5', 3);",
                    "insert into book (ISBN, title, author_id) values ('1248515155', 'Livre 6', 3);",
                    "insert into book (ISBN, title, author_id) values ('1952650225', 'Livre 7', 4);",
                    "insert into book (ISBN, title, author_id) values ('184485155', 'Livre 8', 4);",
            };
            Statement statement = this.connection.createStatement();
            for (String sql : populate) {
                statement.execute(sql);
            }
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        }
    }
}
