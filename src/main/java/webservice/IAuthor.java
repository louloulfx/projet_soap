package webservice;

import entities.Author;
import entities.Authors;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface IAuthor {
    @WebMethod
    Authors getAuthors();

    @WebMethod
    Author getAuthorById(Integer id);

    @WebMethod
    String createAuthor(String prenom, String nom);

    @WebMethod
    String updateAuthor(Integer id, String prenom, String nom);

    @WebMethod
    String deleteAuthor(Integer id);
}