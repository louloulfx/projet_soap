package entities;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Author")
@XmlAccessorType(XmlAccessType.FIELD)
public class Author {

    private Integer id;

    private String prenom;

    private String nom;

    private Books books;

    public Author() {
    }

    public Author(Integer id, String prenom, String nom, Books books) {
        this.id = id;
        this.prenom = prenom;
        this.nom = nom;
        if (books != null) {
            this.books = books;
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Books getBooks() {
        return books;
    }

    public void setBooks(Books books) {
        this.books = books;
    }
}